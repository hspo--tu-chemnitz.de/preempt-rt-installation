# Real-time Linux for Jetson Nano (L4T 32.5)

Information taken from this forum thread:

https://forums.developer.nvidia.com/t/applying-a-preempt-rt-patch-to-jetpack-4-5-on-jetson-nano/168428/4

  

Ubunto 18.04 is highly recommended for this procedure.

Creating the OS image also works well with WSL on windows.

  

After creating an OS image, additional SDK components need to be installed on the Jetson Nano. The NVIDIA SDK Manager is used to perform this task. It has a command-line mode which can be used through docker and WSL, but neither have been tested yet. Only the GUI-mode on an actual install of Ubuntu18.4.6 has been tested so far.

Further information:

WSL: https://docs.nvidia.com/sdk-manager/wsl-systems/index.html

Docker: https://docs.nvidia.com/sdk-manager/docker-containers/index.html

command-line mode: https://docs.nvidia.com/sdk-manager/sdkm-command-line-install/index.html

  

### Install required packages:

```
sudo apt-get update
sudo apt-get install libncurses5-dev
sudo apt-get install build-essential
sudo apt-get install bc
sudo apt-get install lbzip2
sudo apt-get install qemu-user-static
sudo apt-get install python
```

### Create folder

```
mkdir $HOME/jetson_nano
cd $HOME/jetson_nano
```

### Download required files

L4T Jetson Driver Package:

```
wget https://developer.nvidia.com/embedded/L4T/r32_Release_v5.0/T210/Tegra210_Linux_R32.5.0_aarch64.tbz2
```
L4T Sample Root File System:

```
wget https://developer.nvidia.com/embedded/L4T/r32_Release_v5.0/T210/Tegra_Linux_Sample-Root-Filesystem_R32.5.0_aarch64.tbz2
```

L4T Sources:

```
wget https://developer.nvidia.com/embedded/L4T/r32_Release_v5.0/sources/T210/public_sources.tbz2
```
GCC Tool Chain for 64-bit BSP:
```
wget http://releases.linaro.org/components/toolchain/binaries/7.3-2018.05/aarch64-linux-gnu/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu.tar.xz
```

### Extract files

```
sudo tar xpf Tegra210_Linux_R32.5.0_aarch64.tbz2
cd Linux_for_Tegra/rootfs/
sudo tar xpf ../../Tegra_Linux_Sample-Root-Filesystem_R32.5.0_aarch64.tbz2
cd ../../
tar -xvf gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu.tar.xz
sudo tar -xjf public_sources.tbz2
tar -xjf Linux_for_Tegra/source/public/kernel_src.tbz2
```

### Apply real-time patches

```
cd kernel/kernel-4.9/
./scripts/rt-patch.sh apply-patches
```

### Configure and compile the kernel

```
TEGRA_KERNEL_OUT=jetson_nano_kernel
mkdir $TEGRA_KERNEL_OUT
export CROSS_COMPILE=$HOME/jetson_nano/gcc-linaro-7.3.1-2018.05-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
make ARCH=arm64 O=$TEGRA_KERNEL_OUT tegra_defconfig
make ARCH=arm64 O=$TEGRA_KERNEL_OUT menuconfig
```

#### Change these Settings

  

General setup → Timer subsystem → Timer tick handling → Full dynticks system (tickless)

  

Kernel Features → Preemption Model: Fully Preemptible Kernel (RT)

  

Kernel Features → Timer frequency: 1000 HZ

  

#### Compile Kernel

```
make ARCH=arm64 O=$TEGRA_KERNEL_OUT -j4
sudo cp jetson_nano_kernel/arch/arm64/boot/Image $HOME/jetson_nano/Linux_for_Tegra/kernel/Image
sudo cp -r jetson_nano_kernel/arch/arm64/boot/dts/* $HOME/jetson_nano/Linux_for_Tegra/kernel/dtb/
sudo make ARCH=arm64 O=$TEGRA_KERNEL_OUT modules_install INSTALL_MOD_PATH=$HOME/jetson_nano/Linux_for_Tegra/rootfs/
  
cd $HOME/jetson_nano/Linux_for_Tegra/rootfs/
sudo tar --owner root --group root -cjf kernel_supplements.tbz2 lib/modules
sudo mv kernel_supplements.tbz2 ../kernel/
  
cd ..
sudo ./apply_binaries.sh
```

### Generate Jetson Nano image

  

```
cd tools
sudo ./jetson-disk-image-creator.sh -o jetson_nano.img -b jetson-nano -r 300
```

The `-r 300` switch is the revision number of the Jetson Nano module to be used:

100 for revision A01

200 for revision A02

300 for revision B00 or B01

Nothing for Jetson Nano 2GB or Jetson Xavier NX (do not use the -r switch)

  

The resulting image is: `$HOME/jetson_nano/Linux_for_Tegra/tools/jetson_nano.img`

  

### Booting new image

  

Flash the generated image to a microSD Card and perform the initial boot by following the instructions on the official set-up guide:

https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#intro

  

### Installing missing components

  

The generated image does not contain CUDA or any additional Jetson SDK components.

To install them you need to use the NVIDIA SDK Manager, which you can download here:

https://developer.nvidia.com/sdk-manager

  

Follow the steps in the Installation Guide to install the desired components:

https://docs.nvidia.com/sdk-manager/install-with-sdkm-jetson/index.html

Please try Ethernet mode if the board can not be detected.

  

## Testing the installation

(All of these steps can be performed remotely by connecting to the Jetson Nano through SSH)

#### CUDA

To check if CUDA is installed you can run this command

```
cat /usr/local/cuda/version.txt
```

  

#### cyclictest

Source: https://wiki.linuxfoundation.org/realtime/documentation/howto/tools/rt-tests#compile-and-install

  

Run these commands in the terminal to install cyclictest

```
sudo apt-get install build-essential libnuma-dev
  
git clone git://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git
cd rt-tests
git checkout stable/v1.0
make all
make install
  
make cyclictest
```

  

run a basic cyclictest:

Source: https://wiki.linuxfoundation.org/realtime/documentation/howto/tools/cyclictest/start

```
sudo cyclictest --mlockall --smp --priority=80 --interval=200 --distance=0
```
